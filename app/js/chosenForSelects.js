$("#select-region").chosen({
    placeholder_text_single: "Select Region",
    disable_search_threshold: 10
});

$("#select-category").chosen({
    placeholder_text_single: "Select Category",
    disable_search_threshold: 10
});

$("#select-date").chosen({
    placeholder_text_single: "Select Date",
    disable_search_threshold: 10
});

$("#select-price").chosen({
    placeholder_text_single: "Select Price",
    disable_search_threshold: 10
});
