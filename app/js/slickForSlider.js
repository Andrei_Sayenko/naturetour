$(document).ready(function(){
    $('.slider').slick({
        autoplay: 'true',
        autoplaySpeed: 5000,
        fade: true,
        arrows: false,
        dots: true
    });
});